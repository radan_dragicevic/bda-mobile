import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { format } from "date-fns";

export default class Events extends Component {

  constructor(props) {
    super(props);
    this.state = {
      events: [],
      user: props.route.params.user
    };

    fetch("http://88.198.134.26/bda/api/events/myevents/mobile/" + this.state.user.id, {
      method: 'GET'
    })
      .then(response => response.json())
      .then(events => {


        events.map(event => {
          event.timeline = event.startDate[8] + event.startDate[9] + "." +
            event.startDate[5] + event.startDate[6] + "." +
            event.startDate[0] + event.startDate[1] + event.startDate[2] + event.startDate[3] + " " +
            event.startDate[11] + event.startDate[12] + ":" + event.startDate[14] + event.startDate[15] + " - " +
            event.endDate[11] + event.endDate[12] + ":" + event.endDate[14] + event.endDate[15];
          event.location = event.course.city.title.split('":"')[1].split('", "')[0].split('","')[0];
        });

        this.setState({
          events: events
        });
      });
  }

  goTo = (event) => {
    this.props.navigation.navigate('Event', {
      event: event,
      user: this.state.user
    });
  };

  render() {
    return (
      <View style={styles.container}>
        {
          renderIf(this.state.events.length,
            <FlatList
              style={styles.list}
              data={this.state.events}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) =>
                <React.Fragment>
                  <Text onPress={() => this.goTo(item)} style={styles.itemTitle} >{item.type.title}</Text>
                  <Text onPress={() => this.goTo(item)} style={styles.item} >{item.location}</Text>
                  <Text onPress={() => this.goTo(item)} style={styles.item} >{item.timeline}</Text>
                  <Text style={styles.underItem}></Text>
                </React.Fragment>
              }
            />)
        }
        {
          renderIf(!this.state.events.length,
            <Text>You don't have any event at the moment.</Text>)
        }

      </View>
    );
  }
}

function renderIf(condition, content) {
  if (condition) {
    return content;
  } else {
    return null;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    paddingLeft: 10,
    fontSize: 16,
    height: 25,
  },
  itemTitle: {
    padding: 10,
    fontSize: 18,
    height: 42,
    color: "#e57373",
    fontWeight: "bold"
  },
  title: {
    fontSize: 30,
    backgroundColor: "#e57373",
    alignItems: 'center',
    alignSelf: 'stretch',
    color: "white",
    textAlign: "center",
    padding: 10
  },
  underItem: {
    borderBottomColor: "#e57373",
    borderBottomWidth: 1
  },
  list: {
    alignSelf: 'stretch',
  }
});