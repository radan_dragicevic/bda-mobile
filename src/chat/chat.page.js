import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, Button, ScrollView, View } from 'react-native';
import { CheckBox } from 'react-native-elements';

import TranslateService from "../shared/translate.service";
import { parse } from 'date-fns';

export default class ChatPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      event: props.route.params.event,
      user: props.route.params.user,
      comment: "",
      logs: []
    };

    this.checkChat();
  }

  checkChat() {
    var token = `Bearer ${this.state.user.token}`;
    fetch("http://88.198.134.26/bda/api/users/chat/" + this.state.event.id, {
      method: 'GET',
      headers: {
        'Authorization': token
      }
    })
      .then(response => response.json())
      .then(data => {
        this.setState({ logs: data });
      });

      setTimeout(() => this.checkChat(), 5000);
  }

  goTo = () => {

    var chat = {
      UserId: "" + this.state.user.id,
      timestamp: "" + new Date().getTime(),
      text: this.state.comment
    };

    var token = `Bearer ${this.state.user.token}`;
    fetch("http://88.198.134.26/bda/api/users/chat/" + this.state.event.id, {
      method: 'POST',
      body: JSON.stringify(chat),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': token
      }
    })
      .then(response => response.ok)
      .then(response => {
        this.setState({comment: ''});
        this.checkChat();
      });
  };


  render() {
    var myloop = [];

    if (this.state.logs.length) {
      this.state.logs.forEach(log => {
        let date = new Date(parseInt(log.timestamp)).toDateString();

        if (log.userId == this.state.user.id)
          myloop.push(<Text key={log.timestamp} style={styles.currentUserMessage}>{log.text}{"\n"}{date}</Text>);
        else
          myloop.push(<Text key={log.timestamp} style={styles.otherUserMessage}>{log.text}{"\n"}{date}</Text>);
      });
    }

    return (
      <View style={styles.container} >
        {renderIf(this.state.logs.length,

          < ScrollView style={styles.list}>
            {myloop}
          </ScrollView>
        )}
        <Text style={styles.inputTitle}>{TranslateService.translate(global.Language, "report.comment")}</Text>
        <TextInput style={styles.input} value={this.state.comment} onChangeText={(comment) => this.setState({ comment })} />
        <Text style={styles.button} onPress={() => this.goTo()}>{TranslateService.translate(global.Language, "report.submit")}</Text>
      </View >
    )
  }
}



function getMessage(log, userId) {
  if (log.userId == userId)
    return <Text key={log.timestamp} style={styles.currentUserMessage}>{log.timestamp} - {log.text}</Text>
  else
    return <Text key={log.timestamp} style={styles.otherUserMessage}>{log.timestamp} - {log.text}</Text>
}

function renderIf(condition, content) {
  if (condition) {
    return content;
  } else {
    return null;
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  list: {
    alignSelf: 'stretch',
  },
  item: {
    padding: 10,
    fontSize: 16,
    height: 44,
  },
  inputTitle: {
    fontSize: 16,
    alignSelf: 'stretch',
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20
  },
  currentUserMessage: {
    fontSize: 16,
    marginTop: 10,
    marginRight: 100,
    padding: 10,
    color: "white",
    textAlign:"left",
    backgroundColor: "#d6d6d6",
    borderRadius: 10,
    borderColor:"#d6d6d6",
  },
  otherUserMessage: {
    fontSize: 16,
    marginTop: 10,
    marginLeft: 100,
    padding: 10,
    color: "white",
    textAlign:"right",
    backgroundColor: "#d6d6d6",
    borderRadius: 10,
    borderColor:"#d6d6d6",
  },
  input: {
    fontSize: 14,
    borderColor: "gray",
    borderWidth: 1,
    alignSelf: 'stretch',
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    padding: 10
  },
  galleryContainer: {
    bottom: 100
  },
  galleryImageContainer: {
    width: 75,
    height: 75,
    marginRight: 5
  },
  galleryImage: {
    width: 75,
    height: 75
  },
  background: {
    backgroundColor: "#fff"
  },
  button: {
    backgroundColor: "#e57373",
    padding: 10,
    color: "#ffffff",
    width: 150,
    textAlign: "center",
    justifyContent: "center",
    margin: 10,
    alignSelf: "center"
  }
});