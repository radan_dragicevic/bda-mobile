import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, Button, AsyncStorage, FlatList, View } from 'react-native';
import { CheckBox } from 'react-native-elements';

import TranslateService from "../shared/translate.service";
import { parse } from 'date-fns';

export default class ContactsPage extends Component {

  constructor(props) {
    super();

    this.state = {
      user: {},
      contacts: []
    }
  }

  componentDidMount() {
    this.loadUserData();
  }

  loadUserData = async () => {

    const value = await AsyncStorage.getItem('BDA:UserInfo');
    this.setState({ user: JSON.parse(value) });

    var token = `Bearer ${this.state.user.token}`;
    fetch("http://88.198.134.26/bda/api/users/contacts", {
      method: 'GET',
      headers: {
        'Authorization': token
      }
    })
      .then(response => response.json())
      .then(data => {
        this.setState({ contacts: data });
      });
  }

  goTo = (item) => {
    this.props.navigation.navigate('Chat', {
      event: item,
      user: this.state.user
    });

  };


  render() {
    var myloop = [];

    if (this.state.contacts.length) {
      this.state.contacts.forEach(contact => {
        // let date = new Date(parseInt(log.timestamp)).toDateString();

        //  if (log.userId == this.state.user.id)
        myloop.push(<Text key={contact.id} style={styles.currentUserMessage}>{contact.firstName} {contact.lastName}{"\n"}{contact.email}</Text>);
        //   else
        //     myloop.push(<Text key={log.timestamp} style={styles.otherUserMessage}>{log.text}{"\n"}{date}</Text>);
      });
    }

    return (
      <View style={styles.container} >
        {renderIf(this.state.contacts.length,

          <FlatList
            style={styles.list}
            data={this.state.contacts}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) =>
              <React.Fragment>
                <Text onPress={() => this.goTo(item)} style={styles.itemTitle} >{item.firstName} {item.lastName}</Text>
                <Text onPress={() => this.goTo(item)} style={styles.item} >{item.email}</Text>
                <Text style={styles.underItem}></Text>
              </React.Fragment>
            }
          />
        )}
          {renderIf(!this.state.contacts.length,
            <Text>{TranslateService.translate(global.Language, "report.empty")}</Text>
          )}
      </View >
    )
  }
}



function getMessage(log, userId) {
  if (log.userId == userId)
    return <Text key={log.timestamp} style={styles.currentUserMessage}>{log.timestamp} - {log.text}</Text>
  else
    return <Text key={log.timestamp} style={styles.otherUserMessage}>{log.timestamp} - {log.text}</Text>
}

function renderIf(condition, content) {
  return condition ? content : null;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  list: {
    alignSelf: 'stretch',
  },
  item: {
    padding: 5,
    marginLeft: 3,
    fontSize: 16,
    height: 30,
  },
  inputTitle: {
    fontSize: 16,
    alignSelf: 'stretch',
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20
  },
  currentUserMessage: {
    fontSize: 16,
    marginTop: 10,
    marginRight: 100,
    padding: 10,
    color: "white",
    textAlign: "left",
    backgroundColor: "#d6d6d6",
    borderRadius: 10,
    borderColor: "#d6d6d6",
  },
  otherUserMessage: {
    fontSize: 16,
    marginTop: 10,
    marginLeft: 100,
    padding: 10,
    color: "white",
    textAlign: "right",
    backgroundColor: "#d6d6d6",
    borderRadius: 10,
    borderColor: "#d6d6d6",
  },
  input: {
    fontSize: 14,
    borderColor: "gray",
    borderWidth: 1,
    alignSelf: 'stretch',
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    padding: 10
  },
  galleryContainer: {
    bottom: 100
  },
  galleryImageContainer: {
    width: 75,
    height: 75,
    marginRight: 5
  },
  galleryImage: {
    width: 75,
    height: 75
  },
  background: {
    backgroundColor: "#fff"
  },
  button: {
    backgroundColor: "#e57373",
    padding: 10,
    color: "#ffffff",
    width: 150,
    textAlign: "center",
    justifyContent: "center",
    margin: 10,
    alignSelf: "center"
  },
  itemTitle: {
    padding: 5,
    marginLeft: 3,
    fontSize: 18,
    height: 30,
    color: "#e57373",
    fontWeight: "bold"
  },
  title: {
    fontSize: 30,
    backgroundColor: "#e57373",
    alignItems: 'center',
    alignSelf: 'stretch',
    color: "white",
    textAlign: "center",
  },
  underItem: {
    borderBottomColor: "#e57373",
    borderBottomWidth: 1
  }
});