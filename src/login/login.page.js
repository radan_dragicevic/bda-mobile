import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, AsyncStorage, Picker, Platform, ScrollView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import TranslateService from "../shared/translate.service";
import { Updates } from 'expo';

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      language: "en",
      usernameError: false,
      passwordError: false,
      loading: false
    };

  }

  componentDidMount() {
    this.loadUserData();
  }

  loadUserData = async () => {
    try {
      const value = await AsyncStorage.getItem('BDA:UserLoginInfo');
      if (value !== null) {
        var parsedValue = JSON.parse(value);
        this.state = {
          username: parsedValue.email,
          password: parsedValue.password,
          language: parsedValue.language
        };
        this.setState({
          username: parsedValue.email,
          password: parsedValue.password,
          language: parsedValue.language
        });
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  loginUser = () => {
    this.loading = true;
    var body = {
      "email": this.state.username,
      "password": this.state.password
    }

    fetch("http://88.198.134.26/bda/api/users/login", {
      method: 'POST',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    })
      .then(response => response.json())
      .then(response => {
        this.storeUserData(response, body);
      })
  };

  storeUserData = async (response, body) => {
    if (response.status == 404) {
      this.setState({
        usernameError: true
      });
    } else if (response.status == 401) {
      this.setState({
        passwordError: true
      });
    } else {
      try {
        body.language = this.state.language;
        await AsyncStorage.setItem('BDA:UserLoginInfo', JSON.stringify(body));
        await AsyncStorage.setItem('BDA:UserInfo', JSON.stringify(response));

        global.CurrentUser = body;
        global.Language = body.language;
        global.Token = body.token;
      } finally {
        this.props.navigation.navigate('Events', {
          user: response,
        });
      }
    }
  };

  render() {

    return (
      <ScrollView style={styles.container}>
        <Image source={require("../../assets/logo.png")} style={styles.logo} />
        <View style={styles.inputContainer}>
          <TextInput style={this.state.usernameError ? styles.error : styles.inputEmail} placeholder={TranslateService.translate(this.state.language, "login.username")} value={this.state.username} onChangeText={(username) => this.setState({ username })} />
          <TextInput style={this.state.passwordError ? styles.error : styles.inputPassword} placeholder={TranslateService.translate(this.state.language, "login.password")} secureTextEntry={true} value={this.state.password} onChangeText={(password) => this.setState({ password })} />
          <Text style={styles.label}></Text>
          {renderIf(Platform.OS == "android",
            <View style={styles.border}>
              <Picker
                selectedValue={this.state.language}
                onValueChange={language => {
                  this.setState({ language });
                  global.Language = language;
                }}
                style={styles.picker}
                mode="dropdown"
                itemStyle={styles.input}>
                <Picker.Item label="English" value="en" />
                <Picker.Item label="Pусский" value="ru" />
                <Picker.Item label="Српски" value="srb" />
                <Picker.Item label="Srpski latinica" value="srb_lat" />
              </Picker>
            </View>
          )}
          {renderIf(Platform.OS != "android",
            <View style={styles.pickerIOS}>
              <Picker
                selectedValue={this.state.language}
                onValueChange={language => {
                  this.setState({ language });
                  global.Language = language;
                }}
                mode="dropdown"
                itemStyle={styles.input}>
                <Picker.Item label="English" value="en" />
                <Picker.Item label="Pусский" value="ru" />
                <Picker.Item label="Српски" value="srb" />
                <Picker.Item label="Srpski latinica" value="srb_lat" />
              </Picker>
            </View>
          )}
        </View>
        {renderIf(
          !this.state.loading,
          <Text style={styles.button} onPress={this.loginUser}>{TranslateService.translate(this.state.language, "login.login")}</Text>
        )}
        {renderIf(
          this.state.loading,
          <Text style={styles.button}><Image source={require("../../assets/loader_gif.gif")} style={styles.smallIcon} /></Text>
        )}
      </ScrollView>
    );
  }
}

function renderIf(condition, content) {
  return condition ? content : null;
}

//var {Platform} = React;
const styles = StyleSheet.create({
  logo: {
    flex: 0,
    height: 100,
    width: 300,
    resizeMode: 'contain',
    marginLeft: 20,
    marginTop: 50,
  },
  inputContainer: {
    marginTop: 50,
    flex: 0,
  },
  smallIcon: {
    resizeMode: 'contain'
  },
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  title: {
    fontSize: 20,
    textAlign: "left",
    marginBottom: 40
  },
  inputEmail: {
    fontSize: 14,
    borderColor: "black",
    borderWidth: 1,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    marginBottom: -1,
    padding: 10,
    borderTopRightRadius: 3,
    borderTopLeftRadius: 3,
  },
  inputPassword: {
    fontSize: 14,
    borderColor: "black",
    borderWidth: 1,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    marginBottom: -1,
    padding: 10,
    borderBottomRightRadius: 3,
    borderBottomLeftRadius: 3,
  },
  input: {
    fontSize: 14,
    borderColor: "black",
    borderWidth: 1,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    marginBottom: -1,
    padding: 10,
    borderRadius: 3,
  },
  border: {
    borderColor: "black",
    borderWidth: 1,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
    borderRadius: 3
  },
  pickerIOS: {
    height: 250
  },
  picker: {
    fontSize: 14,
    borderRadius: 3,
    borderColor: "black",
    borderWidth: 1
  },
  button: {
    backgroundColor: "#e57373",
    padding: 10,
    color: "#ffffff",
    textAlign: "center",
    justifyContent: "center",
    marginTop: 20,
    borderRadius: 3,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20
  },
  error: {
    fontSize: 14,
    borderWidth: 1,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 10,
    borderColor: "red",
    padding: 3
  },
  label: {
    textAlign: "left",
    alignSelf: "stretch",
    marginLeft: 20
  }
});