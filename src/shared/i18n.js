import i18n from 'i18next';
import Expo from 'expo';

i18n
  .init({
    fallbackLng: 'en',
// the translations
    // realworld load that via xhr or bundle those using webpack    
    resources: {
      en: {
        home: {
          title: 'Welcome'
        }
      },
      de: {
        home: {
          title: 'Willkommen'
        }
      },
// have a initial namespace
      ns: ['translation'],
      defaultNS: 'translation'
    }
  });
export default i18n;