const TranslateService = {
    translate(language, value) {
        var value1 = value.split('.')[0];
        var value2 = value.split('.')[1];

        if (language)
            return translations[language][value1][value2];
        else 
            return  translations['en'][value1][value2];
    }
};

export default TranslateService;

const translations = {
    "en": {
        "navigation": {
            "my_events": "My Events",
            "event": "Event",
            "take_picture": "Take Picture",
            "gallery": "Gallery",
            "image": "Image",
            "take_video": "Take Video",
            "video": "Video",
            "report": "Report",
            "chat": "Chat"
        },
        "login": {
            "title": "LOGIN TO YOUR ACCOUNT",
            "username": "E-mail",
            "password": "Password",
            "language": "Language",
            "login": "Login"
        },
        "event": {
            "title": "Title",
            "course": "Course",
            "location": "Location",
            "time": "Time",
            "type": "Type",
            "description": "Description",
            "gallery": "Gallery",
            "report": "Report",
            "chat": "Chat",
            "date": "Date",
            "patient": "Patient",
            "level": "Level",
            "contact": "Contact"
        },
        "gallery": {
            "add_picture": "Take Picture",
            "from_storage": "Select Picture",
            "add_video": "Add Video"
        },
        "report": {
            "q1": "1. Parents not interested",
            "q2": "2. No parents came (grandma, grandpa, assistant, some relatives came ...)",
            "q3": "3. Hard to work with the child (autism, cat syndrome, spoiled ..)",
            "q4": "4. Mother is pregnant",
            "q5": "5. Parents resentful of therapies they have attended",
            "q6": "6. Time management (can't work for a long time due to obligations, they still have a lot of children .....)",
            "q7": "7. Poor communication, they don't seem to have understood the explanations regarding the therapy",
            "q8": "8. Have unrealistic expectations of therapy",
            "q9": "9. The mother has no support for therapy from the family",
            "q10": "10. They live in a small place, it is difficult to procure materials (try to provide them with everything on the course to buy)",
            "q11": "11. Parents attended ABR course",
            "q12": "12. Parents were not in the first lesson",
            "q13": "13. They want to finish the exercises as soon as possible, without the desire to practice a lot in the training itself",
            "q14": "14. Parents are afraid that the exercises will potentially harm the child (due to stoma, shanta, epilepsy ...)",
            "q15": "15. Parents apply a lot of other therapies during the day.",
            "comment": "Comment",
            "submit": "Submit",
            "empty": "There is no one to chat with"
        }
    },
    "ru": {
        "navigation": {
            "my_events": "Мои события",
            "event": "Событие",
            "take_picture": "Сделать фотографию",
            "gallery": "Галерея",
            "image": "Фотографию",
            "take_video": "Взять видео",
            "video": "Видео",
            "report": "Oтчет",
            "chat": "Чат"
        },
        "login": {
            "title": "ВОЙДИТЕ В СВОЙ АККАУНТ",
            "username": "е-мейл",
            "password": "Пароль",
            "language": "Язык",
            "login": "Авторизоваться"
        },
        "event": {
            "title": "Имя",
            "course": "Курс",
            "location": "Pасположение",
            "time": "Время",
            "type": "Тип",
            "description": "Описание",
            "gallery": "Галерея",
            "report": "Oтчет",
            "chat": "Чат",
            "date": "Дата"
        },
        "gallery": {
            "add_picture": "Усликај изображение",
            "from_storage": "Выберите изображение",
            "add_video": "Добавить видео"
        },
        "report": {
            "q1": "1. Родители не заинтересованы",
            "q2": "2. Родители не пришли (бабушка, дедушка, помощник, некоторые родственники пришли ...)",
            "q3": "3. Трудно работать с ребенком (аутизм, кошачий синдром, испорченный ..)",
            "q4": "4. Majka trudna",
            "q5": "5. Родители обижены на терапию, которую они посетили",
            "q6": "6. Управление временем (я не могу работать долго из-за обязательств, у них все еще много детей .....)",
            "q7": "7. Плохое общение. Похоже, они не поняли объяснений относительно терапии",
            "q8": "8. Имейте нереальные ожидания от терапии",
            "q9": "9. Мать не поддерживает терапию из семьи",
            "q10": "10. Они живут в небольшом месте, трудно закупать материалы (постарайтесь обеспечить их всем, что нужно купить)",
            "q11": "11. Родители посещали курс ABR",
            "q12": "12. Родители не были на первом уроке",
            "q13": "13. Они хотят закончить упражнения как можно скорее, без желания много практиковаться в самой тренировке",
            "q14": "14. Родители опасаются, что упражнения могут навредить ребенку (из-за стомы, шанты, эпилепсии ...)",
            "q15": "15. Родители применяют множество других методов лечения в течение дня",
            "comment": "Комментарий",
            "submit": "Разместить",
            "empty": "Не с кем поговорить"
        }
    },
    "srb": {
        "navigation": {
            "my_events": "Мојa дешавања",
            "event": "Дешавање",
            "take_picture": "Додај слику",
            "gallery": "Галерија",
            "image": "Слика",
            "take_video": "Додај видео",
            "video": "Видео",
            "report": "Упитник",
            "chat": "Ћаскање"
        },
        "login": {
            "title": "УНЕСИТЕ СВОЈ НАЛОГ",
            "username": "Е-маил",
            "password": "Лозинка",
            "language": "Језик",
            "login": "Потврди"
        },
        "event": {
            "title": "Назив",
            "course": "Курс",
            "location": "Локација",
            "time": "Време",
            "type": "Тип",
            "description": "Опис",
            "gallery": "Галерија",
            "report": "Упитник",
            "chat": "Ћаскање",
            "date": "Датум"
        },
        "gallery": {
            "add_picture": "Усликај слику",
            "from_storage": "Oдаберите  слику",
            "add_video": "Додај видео"
        },
        "report": {
            "q1": "1. Родитељи незаинтересовани",
            "q2": "2. Нису дошли родитељи (досли баба, деда, помоћница, неко од родбине ....)",
            "q3": "3. Тешко радити са дететом (аутизам, мачка синдром, размажено ..)",
            "q4": "4. Мајка трудна",
            "q5": "5. Родитељи огорчени на терапијама који су посетили",
            "q6": "6. Менаџмент времена (не могу дуго да радим због обавеза, имаћу доста деце .....)",
            "q7": "7. Лоша комуникација, не делујући као да су схватили објаве везано за терапију",
            "q8": "8. Имају нереална очекивања од терапије",
            "q9": "9. Мајка нема подршку за терапију породице",
            "q10": "10. Живе у малом месту, обећајте набавку материјала (потрудите се да им обезбедите све на курсу да купе)",
            "q11": "11. Родитељи су посецивали АБР курс",
            "q12": "12. Родитељи нису били на првом лекцији",
            "q13": "13. Ћеле да се заврше са везама, а железо не може да обуче доста везбају",
            "q14": "14. Родитељи уплашени да це веббама потенцијално наудити детету (због стоме, шанте, епилепсије ..)",
            "q15": "15. Родитељи примењују доста других терапија током дана.",
            "comment": "Komentar",
            "submit": "Потврди",
            "empty": "Нема са ким да ћаска"
        }
    },
    "srb_lat": {
        "navigation": {
            "my_events": "Moja dešavanje",
            "event": "Dešavanje",
            "take_picture": "Dodaj sliku",
            "gallery": "Galerija",
            "image": "Slika",
            "take_video": "Dodaj video",
            "video": "Video",
            "report": "Upitnik",
            "chat": "Ćaskaj"
        },
        "login": {
            "title": "UNESITE SVOJ NALOG",
            "username": "E-mail",
            "password": "Lozinka",
            "language": "Jezik",
            "login": "Potvrdi"
        },
        "event": {
            "title": "Naziv",
            "course": "Kurs",
            "location": "Lokacija",
            "time": "Vrijeme",
            "type": "Tip",
            "description": "Opis",
            "gallery": "Galerija",
            "report": "Upitnik",
            "chat": "Ćaskaj",
            "date": "Datum"
        },
        "gallery": {
            "add_picture": "Uslikaj sliku",
            "from_storage": "Odaberite sliku",
            "add_video": "Dodaj video"
        },
        "report": {
            "q1": "1. Roditelji nezainteresovani",
            "q2": "2. Nisu došli roditelji( dosli baba, deda, pomoćnica, neko od rodbine....)",
            "q3": "3. Teško raditi sa detetom( autizam, mačka sindrom, razmaženo..)",
            "q4": "4. Majka trudna",
            "q5": "5. Roditelji ogorčeni na terapije koje su posećivali",
            "q6": "6. Menadžment vremena(ne mogu dugo da rade zbog obaveza,imaju jos dosta dece.....)",
            "q7": "7. Loša komunikacija, ne deluju kao da su shvatili objašnjenja vezano za terapiju",
            "q8": "8. Imaju nerealna očekivanja od terapije",
            "q9": "9. Majka nema podršku za terapiju od porodice",
            "q10": "10. Žive u malom mestu,otezana nabavka materijala(potruditi se da im se obezbedi sve na kursu da kupe)",
            "q11": "11. Roditelji posecivali ABR kurs",
            "q12": "12. Roditelji nisu bili na prvoj lekciji",
            "q13": "13. Ćele sto pre da zavrse sa vezbama, bez želje da na samoj obuci dosta vezbaju",
            "q14": "14. Roditelji uplašeni da ce vezbama potencijalno nauditi detetu(zbog stome,shanta,epilepsije..)",
            "q15": "15. Roditelji primenjuju dosta drugih terapija tokom dana.",
            "comment": "Komentar",
            "submit": "Potvrdi",
            "empty": "Nema s kim razgovarati"
        }
    }
}