import React, { Component } from 'react';
import { Text, AsyncStorage } from 'react-native';

export default class TranslatedText extends Component {
  constructor() {
    super();
    this.state = {
      language: 'en'
    };
    this.loadUserData();
  }

  loadUserData = async () => {
    const value = await AsyncStorage.getItem('BDA:UserLoginInfo');
    console.log('state ', value);
    var lang = value.language;
    this.setState({language: lang});
    console.log('state ', this.state);
  }

  render() {
    var text = '';
    console.log(this.state);

    if (this.state.language == 'en') {
      text = "Login";
    } else if (this.state.language == 'ru') {
      text = "qwe";
    }

    return (
      <Text>{text}wq</Text>
    );
  }

}