import React, { Component } from 'react';
import { StyleSheet, FlatList, Text, View, Button, ScrollView, Image } from 'react-native';
import CameraPage from './camera/camera.page';

import TranslateService from "../shared/translate.service";
import { parseJSON } from 'date-fns';

export default class Event extends Component {

  constructor(props) {
    super(props);
    var event = props.route.params.event;

    event.location = event.course.city.title.split('":"')[1].split('", "')[0].split('","')[0] + ", " + event.course.city.country.title.split('":"')[1].split('", "')[0].split('","')[0];
    event.date = event.startDate[8] + event.startDate[9] + "." +
      event.startDate[5] + event.startDate[6] + "." +
      event.startDate[0] + event.startDate[1] + event.startDate[2] + event.startDate[3];
    event.time = event.startDate[11] + event.startDate[12] + ":" + event.startDate[14] + event.startDate[15] + " - " +
      event.endDate[11] + event.endDate[12] + ":" + event.endDate[14] + event.endDate[15];
    if (event.description) event.Content = JSON.parse(event.description);

    this.state = {
      event: event,
      user: props.route.params.user,
      captures: []
    };
  }

  goTo = (event, location) => {
    this.props.navigation.navigate(location, {
      event: event,
      user: this.state.user
    });
  };


  render() {

    var myloop = [];

    if (this.state.event.Content) {
      for (let i = 0; i < this.state.event.Content.length; i++) {
        if (this.state.event.Content[i].Type == "Title")
          myloop.push(
            <Text style={styles.inputTitle} key={this.state.event.Content[i].Content}>{this.state.event.Content[i].Content}</Text>
          );
        else if (this.state.event.Content[i].Type == "Image")
          myloop.push(
            <Image source={{ uri: this.state.event.Content[i].Content }} style={styles.galleryImage} resizeMode="center" key={this.state.event.Content[i].Content} />
          );
        else
          myloop.push(
            <Text style={styles.input}>{this.state.event.Content[i].Content} key={this.state.event.Content[i].Content}</Text>
          );
      }
    }

    // <Text style={styles.button} onPress={() => this.goTo(this.state.event, "Report")}>{TranslateService.translate(global.Language, "event.report")}</Text>
    return (
      <ScrollView style={styles.container}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <Text style={styles.inputTitle}>{TranslateService.translate(global.Language, "event.type")}</Text>
          <Text style={styles.inputTitle}>{TranslateService.translate(global.Language, "event.course")}</Text>
        </View>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <Text style={styles.input}>{this.state.event.type.title}</Text>
          <Text style={styles.input}>{this.state.event.course.title}</Text>
        </View>
        <Text style={styles.underItem}></Text>

        <Text style={styles.inputTitle}>{TranslateService.translate(global.Language, "event.patient")}</Text>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <Text style={styles.input}>{this.state.event.patient.firstName}</Text>
          <Text style={styles.input}>{this.state.event.patient.lastName}</Text>
        </View>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <Text style={styles.inputTitle}>{TranslateService.translate(global.Language, "event.level")}</Text>
          <Text style={styles.inputTitle}>{TranslateService.translate(global.Language, "event.contact")}</Text>
        </View>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <Text style={styles.input}>{this.state.event.patient.courseLevel}</Text>
          <Text style={styles.input}>{this.state.event.patient.contactNumber}</Text>
        </View>
        <Text style={styles.underItem}></Text>

        <View style={{ flex: 1, flexDirection: 'row' }}>
          <Text style={styles.inputTitle}>{TranslateService.translate(global.Language, "event.date")}</Text>
          <Text style={styles.inputTitle}>{TranslateService.translate(global.Language, "event.time")}</Text>
        </View>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <Text style={styles.input}>{this.state.event.date}</Text>
          <Text style={styles.input}>{this.state.event.time}</Text>
        </View>

        <Text style={styles.inputTitle}>{TranslateService.translate(global.Language, "event.location")}</Text>
        <Text style={styles.input}>{this.state.event.location}</Text>

        <View style={styles.footer}>
          <Text style={styles.button} onPress={() => this.goTo(this.state.event, "Gallery")}>{TranslateService.translate(global.Language, "event.gallery")}</Text>
          {renderIf(this.state.event.therapist,
            <Text style={styles.button} onPress={() => this.goTo(this.state.event, "Chat")}>{TranslateService.translate(global.Language, "event.chat")}</Text>
          )}
        </View>
      </ScrollView>
    )
  };
}

function renderIf(condition, content) {
  return condition ? content : null;
}


const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff'
  },
  underItem: {
    borderBottomColor: "#e57373",
    borderBottomWidth: 1,
    marginLeft: 10,
    marginRight: 10
  },
  cube: {
    width: '50%'
  },
  item: {
    padding: 10,
    fontSize: 16,
    height: 44,
  },
  inputTitle: {
    fontSize: 16,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    fontWeight: "bold",
    color: "#e57373",
    width: "40%"
  },
  input: {
    fontSize: 16,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    width: "40%"
  },
  galleryContainer: {
    bottom: 100
  },
  galleryImageContainer: {
    width: 75,
    height: 75,
    marginRight: 5
  },
  galleryImage: {
    width: 100,
    height: 100,
    marginLeft: 20,
    marginRight: 20
  },
  button: {
    backgroundColor: "#e57373",
    padding: 10,
    color: "#ffffff",
    width: 150,
    textAlign: "center",
    justifyContent: "center",
    margin: 20,
    marginTop: 50,
    borderRadius: 3
  },
  footer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: "space-between"
  }
});

