import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, Button, ScrollView, Image } from 'react-native';
import { CheckBox } from 'react-native-elements';

import TranslateService from "../shared/translate.service";

export default class ReportPage extends Component {

  constructor(props) {
    super(props);
    var event = props.route.params.event;

    this.state = {
      event: event,
      Q1: false,
      Q2: false,
      Q3: false,
      Q4: false,
      Q5: false,
      Q6: false,
      Q7: false,
      Q8: false,
      Q9: false,
      Q10: false,
      Q11: false,
      Q12: false,
      Q13: false,
      Q14: false,
      Q15: false,
      comment: ""
    };

  }

  goTo = (event) => {
    var questions = {
      "Q1": this.state.Q1,
      "Q2": this.state.Q2,
      "Q3": this.state.Q3,
      "Q4": this.state.Q4,
      "Q5": this.state.Q5,
      "Q6": this.state.Q6,
      "Q7": this.state.Q7,
      "Q8": this.state.Q8,
      "Q9": this.state.Q9,
      "Q10": this.state.Q10,
      "Q11": this.state.Q11,
      "Q12": this.state.Q12,
      "Q13": this.state.Q13,
      "Q14": this.state.Q14,
      "Q15": this.state.Q15,
      "Comment": this.state.comment
    };

    var report = {
      event_Id: this.state.event.id,
      title: "Evaluation",
      data: JSON.stringify(questions)
    };

    fetch("http://88.198.134.26/bda/api/reports", {
      method: 'POST',
      body: JSON.stringify(report),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then(response => {
        this.props.navigation.navigate('Event', {
          event: this.state.event
        });
      });
  };


  render() {
    var q1Questions = TranslateService.translate(global.Language, "report.q1");
    var q2Questions = TranslateService.translate(global.Language, "report.q2");
    var q3Questions = TranslateService.translate(global.Language, "report.q3");
    var q4Questions = TranslateService.translate(global.Language, "report.q4");
    var q5Questions = TranslateService.translate(global.Language, "report.q5");
    var q6Questions = TranslateService.translate(global.Language, "report.q6");
    var q7Questions = TranslateService.translate(global.Language, "report.q7");
    var q8Questions = TranslateService.translate(global.Language, "report.q8");
    var q9Questions = TranslateService.translate(global.Language, "report.q9");
    var q10Questions = TranslateService.translate(global.Language, "report.q10");
    var q11Questions = TranslateService.translate(global.Language, "report.q11");
    var q12Questions = TranslateService.translate(global.Language, "report.q12");
    var q13Questions = TranslateService.translate(global.Language, "report.q13");
    var q14Questions = TranslateService.translate(global.Language, "report.q14");
    var q15Questions = TranslateService.translate(global.Language, "report.q15");

    return (
      <ScrollView style={[styles.background]}>
        <CheckBox
          title={q1Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q1}
          onPress={() => this.setState({ Q1: !this.state.Q1 })}
        />
        <CheckBox
          title={q2Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q2}
          onPress={() => this.setState({ Q2: !this.state.Q2 })}
        />
        <CheckBox
          title={q3Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q3}
          onPress={() => this.setState({ Q3: !this.state.Q3 })}
        />
        <CheckBox
          title={q4Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q4}
          onPress={() => this.setState({ Q4: !this.state.Q4 })}
        />
        <CheckBox
          title={q5Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q5}
          onPress={() => this.setState({ Q5: !this.state.Q5 })}
        />
        <CheckBox
          title={q6Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q6}
          onPress={() => this.setState({ Q6: !this.state.Q6 })}
        />
        <CheckBox
          title={q7Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q7}
          onPress={() => this.setState({ Q7: !this.state.Q7 })}
        />
        <CheckBox
          title={q8Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q8}
          onPress={() => this.setState({ Q8: !this.state.Q8 })}
        />
        <CheckBox
          title={q9Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q9}
          onPress={() => this.setState({ Q9: !this.state.Q9 })}
        />
        <CheckBox
          title={q10Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q10}
          onPress={() => this.setState({ Q10: !this.state.Q10 })}
        />
        <CheckBox
          title={q11Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q11}
          onPress={() => this.setState({ Q11: !this.state.Q11 })}
        />
        <CheckBox
          title={q12Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q12}
          onPress={() => this.setState({ Q12: !this.state.Q12 })}
        />
        <CheckBox
          title={q13Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q13}
          onPress={() => this.setState({ Q13: !this.state.Q13 })}
        />
        <CheckBox
          title={q14Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q14}
          onPress={() => this.setState({ Q14: !this.state.Q14 })}
        />
        <CheckBox
          title={q15Questions}
          checkedColor='red'
          containerStyle={[styles.background]}
          checked={this.state.Q15}
          onPress={() => this.setState({ Q15: !this.state.Q15 })}
        />
        <Text style={styles.inputTitle}>{TranslateService.translate(global.Language, "report.comment")}</Text>
        <TextInput  style={styles.input} value={this.state.comment} onChangeText={(comment) => this.setState({ comment })} />
        <Text style={styles.button} onPress={() => this.goTo(this.state.event)}>{TranslateService.translate(global.Language, "report.submit")}</Text>
      </ScrollView>
    )
  };
}

function renderIf(condition, content) {
  if (condition) {
    return content;
  } else {
    return null;
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    padding: 10,
    fontSize: 16,
    height: 44,
  },
  inputTitle: {
    fontSize: 16,
    alignSelf: 'stretch',
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20
  },
  input: {
    fontSize: 14,
    borderColor: "gray",
    borderWidth: 1,
    alignSelf: 'stretch',
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    padding: 10
  },
  galleryContainer: {
    bottom: 100
  },
  galleryImageContainer: {
    width: 75,
    height: 75,
    marginRight: 5
  },
  galleryImage: {
    width: 75,
    height: 75
  },
  background: {
    backgroundColor: "#fff"
  },
  button: {
    backgroundColor: "#e57373",
    padding: 10,
    color: "#ffffff",
    width: 150,
    textAlign: "center",
    justifyContent: "center",
    margin: 10,
    alignSelf: "center"
  }
});