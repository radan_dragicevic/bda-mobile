import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Camera } from 'expo-camera';
import * as VideoThumbnails from 'expo-video-thumbnails';
import * as DocumentPicker from 'expo-document-picker';

import TranslateService from "../../shared/translate.service";

export default class Gallery extends Component {

  constructor(props) {
    super(props);

    this.state = {
      event: props.route.params.event,
      captures: [],
      videos: []
    };

    console.log(this.state);


  }

  componentDidMount() {
    setTimeout(() => {

      console.log('event', this.state.event.id);
      var endpoint = "http://88.198.134.26/bda/api/events/files/read/mobile/" + this.state.event.patient.id;

      fetch(endpoint, {
        method: 'GET'
      })
        .then(response => response.json())
        .then(captures => {
          console.log(captures);

          var res = captures
            .filter(c => c.split(".")[c.split(".").length - 1] == "jpg")
            .map(c => "http://" + c);

          console.log(res);
          this.setState({
            captures: res
          });

          this.getTumbnails(captures);
        });
    }, 1000);
  }

  getTumbnails = async (videos) => {

    var res = videos.filter(c => c.split(".")[c.split(".").length - 1] == "mp4");

    var result = [];
    for (var i = 0; i < res.length; i++) {
      var r = await this.generateThumbnail("http://" + res[i]);
      result.push(r);
    }

    console.log(result);

    var videoLinks = res.map(c => "http://" + c);

    this.setState({
      videos: result,
      videoLinks: videoLinks
    });
  }

  generateThumbnail = async (url) => {
    try {
      const { uri } = await VideoThumbnails.getThumbnailAsync(
        url,
        {
          time: 0,
        }
      );
      return uri;
    } catch (e) {
      return null;
    }
  };

  goTo = (event, type) => {
    this.props.navigation.navigate(type, {
      event: event,
    });
  };

  showImage = (image) => {
    this.props.navigation.navigate('ImagePage', {
      image: image,
    });
  };

  showVideo = (index) => {
    console.log(this.state.videoLinks[index]);
    this.props.navigation.navigate('VideoPage', {
      image: this.state.videoLinks[index],
    });
  }

  selectFileFromStorage = () => {
    DocumentPicker.getDocumentAsync()
      .then(result => {
        console.log(result);

        const data = new FormData();
        var timestamp = new Date().getTime();
        var name = "mobileimage_" + timestamp + ".jpg";

        data.append('files', {
          uri: result.uri,
          type: 'image/jpeg', // or photo.type
          name: name
        });
        //+ this.props.event.idss

        fetch("http://88.198.134.26/bda/api/events/files/upload/mobile/" + this.state.event.patient.id, {
          method: 'POST',
          body: data
        })
          .then(response => response.json())
          .then(response => {
            this.props.navigation.navigate('Event', {
              event: this.props.event
            });
          });
      });
  }

  render() {

    return (
      <View style={[styles.background]}>
        {
          renderIf(this.state.captures.length,
            <ScrollView
              horizontal={true}
              style={[styles.input, styles.galleryContainer]}
            >
              {this.state.captures.map(capture => (
                <View style={styles.galleryImageContainer} key={capture}>
                  <TouchableOpacity onPress={() => this.showImage(capture)} >
                    <Image source={{ uri: capture }} style={styles.galleryImage} />
                  </TouchableOpacity>
                </View>
              ))}
            </ScrollView>
          )}
        {
          renderIf(!this.state.captures.length,
            <View style={[styles.input, styles.galleryContainer]}>
              <Image source={require("../../../assets/loader_gif.gif")} style={styles.loader} />
            </View>
          )}
        <View style={styles.galleryContainerButton}>
          <Text style={styles.button_l} onPress={() => this.goTo(this.state.event, "Camera")} >{TranslateService.translate(global.Language, "gallery.add_picture")}</Text>
          <Text style={styles.button_r} onPress={() => this.selectFileFromStorage()} >{TranslateService.translate(global.Language, "gallery.from_storage")}</Text>
          <Text style={styles.underItem}></Text>
        </View>
        {
          renderIf(this.state.videos.length,
            <ScrollView
              horizontal={true}
              style={[styles.input, styles.galleryContainer]}
            >
              {this.state.videos.map((video, index) => (
                <View style={styles.galleryImageContainer} key={video + index}>
                  <TouchableOpacity onPress={() => this.showVideo(index)} >
                    <Image source={{ uri: video }} style={styles.galleryImage} />
                  </TouchableOpacity>
                </View>
              ))}
            </ScrollView>
          )}
        {
          renderIf(!this.state.videos.length,
            <View style={[styles.input, styles.galleryContainer]}>
              <Image source={require("../../../assets/loader_gif.gif")} style={styles.loader} />
            </View>
          )}
        <Text style={styles.button} onPress={() => this.goTo(this.state.event, "Video")} >{TranslateService.translate(global.Language, "gallery.add_video")}</Text>
      </View>
    )
  };
}

function renderIf(condition, content) {
  if (condition) {
    return content;
  } else {
    return null;
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  underItem: {
    borderBottomColor: "#e57373",
    borderBottomWidth: 1,
    margin: 10
  },
  title: {
    fontSize: 30,
    backgroundColor: "#e57373",
    alignItems: 'center',
    alignSelf: 'stretch',
    color: "white",
    textAlign: "center",
    padding: 10
  },
  input: {
    fontSize: 14,
    alignSelf: 'stretch',
    margin: 10
  },
  galleryContainer: {
    bottom: 0
  },
  galleryContainerButton: {
    marginBottom: 0,
    height: 80,
    fontSize: 14,
    alignSelf: 'stretch',
    margin: 10
  },
  galleryImageContainer: {
    width: 150,
    height: 150,
    marginRight: 5
  },
  galleryImage: {
    width: 150,
    height: 150
  },
  loader: {
    alignSelf: 'center',
    alignItems: 'center',
    resizeMode: 'contain',
    height: 150,
  },
  background: {
    backgroundColor: "#fff"
  },
  button: {
    backgroundColor: "#e57373",
    padding: 10,
    color: "#ffffff",
    width: 150,
    textAlign: "center",
    justifyContent: "center",
    alignSelf: "center",
    marginBottom: 100,
    borderRadius: 3
  },
  button_l: {
    backgroundColor: "#e57373",
    padding: 10,
    color: "#ffffff",
    width: 150,
    height: 40,
    textAlign: "center",
    alignSelf: 'flex-start',
    position: 'absolute',
    borderRadius: 3
  },
  button_r: {
    backgroundColor: "#e57373",
    padding: 10,
    color: "#ffffff",
    width: 150,
    height: 40,
    textAlign: "center",
    alignSelf: 'flex-end',
    borderRadius: 3
  }
});