import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, ScrollView, Image } from 'react-native';

export default class ImagePage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      image: props.route.params.image,
    };
  }

  render() {
    return (
      <View>
        <Image source={{ uri: this.state.image }} style={styles.galleryImage} resizeMode="stretch" />
      </View>
    )
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  title: {
    fontSize: 30,
    backgroundColor: "#e57373",
    alignItems: 'center',
    alignSelf: 'stretch',
    color: "white",
    textAlign: "center",
    padding: 10
  },
  input: {
    fontSize: 14,
    alignSelf: 'stretch',
    margin: 20
  },
  galleryContainer: {
    bottom: 100
  },
  galleryImageContainer: {
    width: 500,
    height: 500,
    marginRight: 5
  },
  galleryImage: {
    width: 500,
    height: 500
  }
});