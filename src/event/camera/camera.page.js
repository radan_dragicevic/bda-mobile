import React from 'react';
import { View, Text, Image } from 'react-native';
import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import Toolbar from './toolbar.component';

import styles from './styles';

export default class CameraPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            captures: [],
            flashMode: Camera.Constants.FlashMode.off,
            capturing: null,
            cameraType: Camera.Constants.Type.back,
            hasCameraPermission: null,
            event: props.route.params.event
        };
        console.log("State", this.state);
    }

    camera = null;

    setFlashMode = (flashMode) => this.setState({ flashMode });
    setCameraType = (cameraType) => this.setState({ cameraType });
    handleCaptureIn = () => this.setState({ capturing: true });

    handleCaptureOut = () => {
        if (this.state.capturing)
            this.camera.stopRecording();
    };

    handleShortCapture = async () => {
        const photoData = await this.camera.takePictureAsync({quality: 0.3});
        this.setState({ loading: true, capturing: false, captures: [photoData, ...this.state.captures] });

        const data = new FormData();
        // data.append('name', 'testName'); // you can append anyone.
        var timestamp = new Date().getTime();
        var name = "mobileimage_" + timestamp + ".jpg";

        data.append('files', {
            uri: photoData.uri,
            type: 'image/jpeg', // or photo.type
            name: name
        });
        //+ this.props.event.idss

        fetch("http://88.198.134.26/bda/api/events/files/upload/mobile/" + this.state.event.patient.id, {
            method: 'POST',
            body: data
        })
            .then(response => response.json())
            .then(response => {
                this.props.navigation.navigate('Event', {
                    event: this.props.event
                });
            });
    };

    async componentDidMount() {
        const camera = await Permissions.askAsync(Permissions.CAMERA);
        const audio = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
        const video = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        const hasCameraPermission = (camera.status === 'granted' && audio.status === 'granted' && video.status === 'granted');
        this.setState({ hasCameraPermission });
    };

    render() {
        const { hasCameraPermission, flashMode, cameraType, capturing } = this.state;

        if (hasCameraPermission === null) {
            return <View />;
        } else if (hasCameraPermission === false) {
            return <Text>Access to camera has been denied.</Text>;
        }

        if (this.state.loading) {
            return (
                <View style={styles.container}>
                    <Image source={require("../../../assets/loader_gif.gif")} style={styles.galleryImage} />
                </View>
            );
        } else {
            return (
                <React.Fragment>
                    <View>
                        <Camera type={cameraType}
                            flashMode={flashMode}
                            style={styles.preview}
                            ref={camera => this.camera = camera}>
                        </Camera>
                    </View>
                    <Toolbar
                        capturing={capturing}
                        flashMode={flashMode}
                        cameraType={cameraType}
                        setFlashMode={this.setFlashMode}
                        setCameraType={this.setCameraType}
                        onPress={this.handleShortCapture}
                    />
                </React.Fragment>
            );
        }
    };

};
