import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, ScrollView, Image } from 'react-native';
import CameraPage from './camera/camera.page';

export default class Loader extends Component {

  goTo = (event) => {
    this.props.navigation.navigate('Camera', {
      event: event,
    });
  };


  render() {

    return (
      <View>
        {
          renderIf(this.state.captures.length,
            <ScrollView
              horizontal={true}
              style={[styles.input, styles.galleryContainer]}
            >
              {this.state.captures.map(capture => (
                <View style={styles.galleryImageContainer} key={capture}>
                  <Image source={{ uri: capture }} style={styles.galleryImage} />
                </View>
              ))}
            </ScrollView>
          )}
        <Button color="#e57373" onPress={() => this.goTo(this.state.event)} title="Add Picture" />
      </View>
    )
  };
}

function renderIf(condition, content) {
  if (condition) {
    return content;
  } else {
    return null;
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  title: {
    fontSize: 30,
    backgroundColor: "#e57373",
    alignItems: 'center',
    alignSelf: 'stretch',
    color: "white",
    textAlign: "center",
    padding: 10
  },
  input: {
    fontSize: 14,
    alignSelf: 'stretch',
    margin: 20
  },
  galleryContainer: {
    bottom: 100
  },
  galleryImageContainer: {
    width: 75,
    height: 75,
    marginRight: 5
  },
  galleryImage: {
    width: 75,
    height: 75
  }
});