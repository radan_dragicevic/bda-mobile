import React from 'react';
import { StyleSheet, AsyncStorage, View, Text, Button, Vibration, Platform, Screen } from 'react-native';
import { DrawerActions, NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItem, DrawerItemList } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import { Icon } from 'react-native-elements'

import { Notifications } from 'expo';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import * as BackgroundFetch from 'expo-background-fetch';
import * as TaskManager from 'expo-task-manager';

import Login from './src/login/login.page';
import Events from './src/events-list/events-list.page';
import Event from './src/event/event.page';
import Gallery from './src/event/gallery/gallery.page';
import ImagePage from './src/event/gallery/image.page';
import CameraPage from './src/event/camera/camera.page';
import VideoCameraPage from './src/event/video-camera/video-camera.page';
import VideoPage from './src/event/gallery/video.page';
import ReportPage from './src/event/report.page';

import TranslateService from "./src/shared/translate.service";
import ChatPage from './src/chat/chat.page';
import ContactsPage from './src/chat/contacts.page';

const headerStyle = {
  backgroundColor: '#e57373',
  headerTintColor: "#fff"
}

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

export default class App extends React.Component {
  constructor() {
    super();

    this.state = {
      expoPushToken: '',
      notification: {},
    };

    TaskManager.defineTask('Notification', () => {
      if (global.CurrentUser) {
        fetch("http://88.198.134.26/bda/api/events/pending/" + global.CurrentUser.id, {
          method: 'GET'
        })
          .then(response => response.json())
          .then(events => {
            if (events.length)
              this.sendPushNotification(events[0]);
          });
      }
    });
    if (BackgroundFetch != undefined)
      BackgroundFetch.registerTaskAsync('Notification', { minimumInterval: 43200 });
  }

  registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      token = await Notifications.getExpoPushTokenAsync();
      this.setState({ expoPushToken: token });
    } else {
      alert('Must use physical device for Push Notifications');
    }

    if (Platform.OS === 'android') {
      Notifications.createChannelAndroidAsync('default', {
        name: 'default',
        sound: true,
        priority: 'max',
        vibrate: [0, 250, 250, 250],
      });
    }
  };

  componentDidMount() {
    this.registerForPushNotificationsAsync();
    this._notificationSubscription = Notifications.addListener(this._handleNotification);
  }

  _handleNotification = notification => {
    Vibration.vibrate();
    this.setState({ notification: notification });
  };

  sendPushNotification = async (event) => {
    const message = {
      to: this.state.expoPushToken,
      sound: 'default',
      title: event.title,
      body: 'You have an upcoming event!',
      data: { data: 'goes here' },
      _displayInForeground: true,
    };
    const response = await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),
    });
  };

  render() {

    //this.sendPushNotification();

    var myEvents = TranslateService.translate(global.Language, "navigation.my_events");
    var event = TranslateService.translate(global.Language, "navigation.event");
    var takePicture = TranslateService.translate(global.Language, "navigation.take_picture");
    var gallery = TranslateService.translate(global.Language, "navigation.gallery");
    var image = TranslateService.translate(global.Language, "navigation.image");
    var takeVideo = TranslateService.translate(global.Language, "navigation.take_video");
    var video = TranslateService.translate(global.Language, "navigation.video");
    var report = TranslateService.translate(global.Language, "navigation.report");
    var chat = TranslateService.translate(global.Language, "navigation.chat");
  


    clearAsyncStorage = async () => {
      AsyncStorage.clear();

    }

    const LogoutHeaderButton = () => {
      return (
        <Icon
          reverse
          color="#e57373"
          type="ionicon"
          name="ios-log-out"
          onPress={this.clearAsyncStorage}
        />
      )
    }




    const MyStack = () => {

      return (
        <NavigationContainer independent={true}>
          <Stack.Navigator initialRouteName="Login">
            <Stack.Screen name="Login" component={Login} options={{ title: '', headerStyle: { backgroundColor: '#e57373' }, headerTintColor: "#fff" }} />
            <Stack.Screen name="Events" component={Events} options={({ navigation }) => ({
              title: myEvents,
              headerStyle: { backgroundColor: '#e57373' },
              headerTintColor: "#fff",
              headerLeft: () => {
                return (
                  <Icon
                    reverse
                    name='navicon'
                    type='evilicon'
                    color='#e57373'
                    onPress={() => navigation.toggleDrawer()}
                  />
                )
              },
              headerRight: LogoutHeaderButton
            })} />
            <Stack.Screen name="Event" component={Event} options={{ title: event, headerStyle: { backgroundColor: '#e57373' }, headerTintColor: "#fff" }} />
            <Stack.Screen name="Camera" component={CameraPage} options={{ title: takePicture, headerStyle: { backgroundColor: '#e57373' }, headerTintColor: "#fff" }} />
            <Stack.Screen name="Gallery" component={Gallery} options={{ title: gallery, headerStyle: { backgroundColor: '#e57373' }, headerTintColor: "#fff" }} />
            <Stack.Screen name="ImagePage" component={ImagePage} options={{ title: image, headerStyle: { backgroundColor: '#e57373' }, headerTintColor: "#fff" }} />
            <Stack.Screen name="Video" component={VideoCameraPage} options={{ title: takeVideo, headerStyle: { backgroundColor: '#e57373' }, headerTintColor: "#fff" }} />
            <Stack.Screen name="VideoPage" component={VideoPage} options={{ title: video, headerStyle: { backgroundColor: '#e57373' }, headerTintColor: "#fff" }} />
            <Stack.Screen name="Report" component={ReportPage} options={{ title: report, headerStyle: { backgroundColor: '#e57373' }, headerTintColor: "#fff" }} />
          </Stack.Navigator>
        </NavigationContainer>
      )
    }

    function CustomDrawerContent(props) {
      return (
        <DrawerContentScrollView {...props}>
          <DrawerItemList
            activeBackgroundColor="#e57373"
            activeTintColor="#ffffff"
            color="black"
            {...props} />
        </DrawerContentScrollView>
      )
    }

    const MyStack2 = () => {
      var chat = TranslateService.translate(global.Language, "navigation.chat");
      var contacts = TranslateService.translate(global.Language, "navigation.contacts");
      return (
        <NavigationContainer independent={true}>
          <Stack.Navigator initialRouteName="Contacts">
            <Stack.Screen name="Chat" component={ChatPage} options={{ title: chat, headerStyle: { backgroundColor: '#e57373' }, headerTintColor: "#fff" }} />
            <Stack.Screen name="Contacts" component={ContactsPage} options={({ navigation }) => ({
              title: contacts,
              headerStyle: { backgroundColor: '#e57373' },
              headerTintColor: "#fff",
              headerLeft: () => {
                return (
                  <Icon
                    reverse
                    name='navicon'
                    type='evilicon'
                    color='#e57373'
                    onPress={() => navigation.toggleDrawer()}
                  />
                )
              },
              headerRight: LogoutHeaderButton
            })} />
          </Stack.Navigator>
        </NavigationContainer>
      )
    }
   
    return (
      
      <NavigationContainer>
        <Drawer.Navigator drawerContent={(props) => <CustomDrawerContent {...props} />} initialRouteName="Main" drawerStyle={{ backgroundColor: '#fff' }}>
          <Drawer.Screen name="Main" options={{title: myEvents}} component={MyStack} />
          <Drawer.Screen name="Chat" options={{title: chat}} component={MyStack2} />
        </Drawer.Navigator>
      </NavigationContainer>
    );
  }


}

const styles = StyleSheet.create({
  logoutButton: {
    backgroundColor: '#e57373',
  }
});